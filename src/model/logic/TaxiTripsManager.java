package model.logic;

import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import api.ITaxiTripsManager;
import model.data_structures.Cola;
import model.data_structures.Lista;
import model.data_structures.Pila;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private Pila<Service> stack;
	private Cola<Service> cola;
	
	
	
	public boolean loadServices(String serviceFile, String taxiId) {
		
		stack= new Pila<>();
		cola= new Cola<>();
		
		JsonParser parser = new JsonParser();

		try {
			Object obj = parser.parse(new FileReader(serviceFile));

			JsonArray arregloSer = (JsonArray) obj;

			Iterator<JsonElement> iter = arregloSer.iterator();

			//Itero sobre el arreglo de servicios
			while (iter.hasNext()) {
				
				JsonObject formatoJson = (JsonObject) iter.next();
				
				Service servicio= new Service();
				Taxi taxi= new Taxi();
				
				String company= formatoJson.get("company").getAsString();
				String dropTract= formatoJson.get("dropoff_census_tract").getAsString();
				double droplatitude= formatoJson.get("dropoff_centroid_latitude").getAsDouble();
				
				//droplocation
				JsonObject dropLocation= (JsonObject) formatoJson.get("dropoff_location").getAsJsonObject();
				String typeLocDrop= dropLocation.get("type").getAsString();
				JsonArray arregloLoc= dropLocation.get("coordinates").getAsJsonArray();
				double latLoc= arregloLoc.get(0).getAsDouble();
				double lonLoc= arregloLoc.get(1).getAsDouble();
				
				double droplongitude= formatoJson.get("dropoff_centroid_longitude").getAsDouble();
				int dropcommunity= formatoJson.get("dropoff_community_area").getAsInt();
				int extras= formatoJson.get("extras").getAsInt();
				int fare= formatoJson.get("fare").getAsInt();
				String payment= formatoJson.get("payment_type").getAsString();
				String pickTract= formatoJson.get("pickup_census_tract").getAsString();
				double picklatitude= formatoJson.get("pickup_centroid_latitude").getAsDouble();
				
				//pickLocation
				JsonObject pickLocation= (JsonObject) formatoJson.get("pickup_location").getAsJsonObject();
				String typeLocPick= pickLocation.get("type").getAsString();
				JsonArray arregloLocP= pickLocation.get("coordinates").getAsJsonArray();
				double latLocP= arregloLocP.get(0).getAsDouble();
				double lonLocP= arregloLocP.get(1).getAsDouble();
				
				
				
				double pickLongitude= formatoJson.get("pickup_centroid_longitude").getAsDouble();
				int pickupCommunity= formatoJson.get("pickup_community_area").getAsInt();
				String idTaxi= formatoJson.get("taxi_id").getAsString();
				
				int tips= formatoJson.get("tips").getAsInt();
				int tolls= formatoJson.get("tolls").getAsInt();
				
				
				//tripendtimestamp
				String tripEnd= formatoJson.get("trip_end_timestamp").getAsString();
		
				
				String tripId= formatoJson.get("trip_id").getAsString();
				
				int tripMiles= formatoJson.get("trip_miles").getAsInt();
				int tripSeconds= formatoJson.get("trip_seconds").getAsInt();
				
				//int tripStartTimeStamp= formatoJson.get("trip_start_timestamp").getAsInt();
				String tripStart= formatoJson.get("trip_start_timestamp").getAsString();
		
		
				double tripTotal= formatoJson.get("trip_total").getAsDouble();
				
				taxi.setCompany(company);
				taxi.setTaxiId(idTaxi);
				
				servicio.setTaxiId(idTaxi);
				servicio.setTripId(tripId);
				servicio.setTripMiles(tripMiles);
				servicio.setTripSeconds(tripSeconds);
				servicio.setTripTotal(tripTotal);
				
				
				 //location PICK 
				servicio.setTypeLocPick(typeLocPick);
				servicio.setLatlocP(latLocP);
				servicio.setLonlocP(lonLocP);
				
				//location drop
				servicio.setTypeLocDrop(typeLocDrop);
				servicio.setLatloc(latLoc);
				servicio.setLonloc(lonLoc);
				
				
				servicio.setDropcom(dropcommunity);
				servicio.setPickcom(pickupCommunity);
				
				servicio.setDropTract(dropTract);
				servicio.setPickTract(pickTract);
				
				servicio.setStartTime(tripStart);
				servicio.setEndTime(tripEnd);
				
				servicio.setTolls(tolls);
				servicio.setTips(tips);
				servicio.setExtras(extras);
				servicio.setFare(fare);
				servicio.setPayment(payment);
				
				servicio.setLatitudedrop(droplatitude);
				servicio.setLongdrop(droplongitude);
				
				servicio.setLatitudeP(picklatitude);
				servicio.setLongP(pickLongitude);
				
				
				if(idTaxi.equals(taxiId))
			{
				stack.push(servicio);
				cola.enqueue(servicio);
			}
			
			}
			System.out.println("Inside loadServices with File:" + serviceFile);
			System.out.println("Inside loadServices with TaxiId:" + taxiId);
			return true;
		}
			
			catch(Exception e)
		{
				System.out.println("no se carg�");
				return false; 
		}
	}
	
	public int[] servicesInInverseOrder(String taxiId) {
		
		
		String time="";
		int ordenados=0;
		int desordenados=0;
		Cola<Service> colaAux= new Cola<Service>();
		int [] resultado = new int[2];
		
		while(!stack.isEmpty() && stack.pop().getTaxiId().equalsIgnoreCase(taxiId))
		{
			Service actual= stack.pop();
			
			if(actual.getStartTime().compareTo(time)>0)
				{
					ordenados++;
					time= actual.getStartTime();
					colaAux.enqueue(actual);
				}
				
				else
				{
					desordenados++;
				}
			
		
		}
		
		while(!colaAux.isEmpty())
		{
			stack.push(colaAux.dequeue());
		}
		
		System.out.println("Inside servicesInInverseOrder");
		
		resultado[0]=ordenados;
		resultado[1]=desordenados;
		
		
		return resultado;
		}
	
		
	
		
	

	@Override
	public int [] servicesInOrder(String taxiId) {
		
		int ordenados=0;
		int desordenados=0; 
		int [] resultado = new int[2];
		String tiempo = "";
		Cola<Service> colaAux= new Cola<Service>();
		
		while(!cola.isEmpty() && cola.dequeue().getTaxiId().equalsIgnoreCase(taxiId))
		{
			Service actual= cola.dequeue();
			if(actual.getStartTime().compareTo(tiempo)<0)
			{
				tiempo= actual.getStartTime();
				colaAux.enqueue(actual);
				ordenados++;
			}
			
			else
			{
				desordenados++;
			}
			
		}
		
		
		while(!colaAux.isEmpty())
		{
			cola.enqueue(colaAux.dequeue());
		}
		
		System.out.println("Inside servicesInOrder");
		
		resultado[0]= ordenados;
		resultado[1]= desordenados;
		
		return resultado;
	}






}
