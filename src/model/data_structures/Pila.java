package model.data_structures;

import java.util.Iterator;

public class Pila<K> implements IStack<K>{

	
	public Lista<K> listaStack;
	private int N;
	
	
	public Pila()
	{
		N=0;
		listaStack= new Lista<K>();
	}
	
	
	public void push(K item) {
		
		listaStack.agregarElementoFinal(item);
		N++;
		
	}
	
	
	public int size()
	{
		return N;
	}

	@Override
	public K pop() {
	
		K retorno= listaStack.darElemento(listaStack.darNumeroElementos()-1);
		listaStack.eliminarElemento(listaStack.darNumeroElementos()-1);
		N--;
	
		return retorno;
	}

	@Override
	public boolean isEmpty() {
		
		return listaStack.darNumeroElementos()==0;
	}
	
	
	public Iterator<K> iterator()
	{
		return listaStack.iterator();
	}

}
