package model.data_structures;

import java.util.Iterator;

public class Lista<K> implements ILista<K> {
	// -------------------------------------------------------------
	// Clase interna Nodo Simple.
	// -------------------------------------------------------------

	private class NodoSimple<K> {
		/**
		 * siguiente nodo
		 */
		NodoSimple<K> next;
		/**
		 * item del nodo.
		 */
		K item;
		/**
		 * metodo contructor del nodo.
		 */
		public NodoSimple() {
			next = null;
			item = null;
		}

	}

	// -------------------------------------------------------------
	// Clase principal lista.
	// -------------------------------------------------------------
	/**
	 * referencia a la cabeza de la lista.
	 */
	private NodoSimple<K> head;
	/**
	 * referencia al ultimo elemento de la lista.
	 */
	private NodoSimple<K> last;
	/**
	 * tamanio de la lista.
	 */
	private int size;

	/**
	 * Metodo constructor, inicializa la cabeza de la lista en null.
	 */
	public Lista() {
		head = null;
		last = head;
	}

	/**
	* metodo que retorna un iterador sobre los elementos de la lista.
	*/
	public Iterator<K> iterator() {

		return new Iterator<K>() {

			private NodoSimple<K> actual = head;

			@Override
			public boolean hasNext() {
				return actual != null;
			}

			@Override
			public K next() {
				K item = actual.item;
				actual = actual.next;
				return item;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
		};
	}

	/**
	 * agrega un elemento al inicio de la lista
	 * 
	 * @param elem elemento a agregar.
	 */
	public void agregarElementoInicio(K elem) {
		NodoSimple<K> nuevo = new NodoSimple<>();
		nuevo.item = elem;
		if (head == null) {
			head = nuevo;
			last = head;
		} else {
			nuevo.next = head;
			head = nuevo;
		}
		size++;
	}

	/**
	 * agraga un elemento al final de la lista.
	 * 
	 * @param elem elemento a agregar.
	 */
	public void agregarElementoFinal(K elem) {
		NodoSimple<K> newNode = new NodoSimple<K>();
		newNode.item = elem;
		if (head == null) {
			head = newNode;
			last = head;
		} else {
			last.next = newNode;
			last = newNode;

		}
		size++;
	}

	/**
	 * @return el elemento en la lista segun la posicion dada por parametro
	 * @param pos posicion que se desa encontrar
	 */
	public K darElemento(int pos) {
		if (pos < 0 || pos >= size) {
			throw new ArrayIndexOutOfBoundsException(pos);
		} else if (pos == 0) {
			return head.item;
		} else {
			NodoSimple<K> recorrido = head;
			for (int newPos = 0; newPos < pos; newPos++)
				recorrido = recorrido.next;
			return recorrido.item;
		}
	}

	/**
	 * elimina el primer elemento de la lista.
	 * @return el primer elemento de la lista eliminado.
	 */
	public K eliminarPrimero() {
		if (head == null)
			throw new NullPointerException();
		K item = head.item;
		head = head.next;
		size--;
		if (size == 0) {
			head = null;
		}
		return item;

	}

	/**
	 * @return el tamanio de la lista.
	 */
	public int darNumeroElementos() {
		return size;
	}

	/**
	 * retorna el ultimo elemento de la lista.
	 * @return ultimo elemento de la lista.
	 */
	public K darUltimoElemento() {
		return last.item;
	}

	/**
	 * retorna el primero elemento de la lista.
	 * @return primero elemento de la lista.
	 */
	public K darPrimerElemento() {
		return head.item;
	}
	
	/**
	 * elimina el elemento de la lista segun la posicion indicada
	 * @return ultimo elemento de la lista.
	 */
	// TODO Documentar.
	
	public K eliminarElemento(int pos) {
		if (head == null)
			throw new NullPointerException();
		else {
			NodoSimple<K> current = head;
			for (int newPos = 0; newPos <= pos - 1; newPos++)
				current = current.next;
			K rta = current.next.item;
			current.next = current.next.next;
			size--;
			return rta;
		}
	}


}
