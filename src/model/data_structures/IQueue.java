package model.data_structures;

public interface IQueue<K> extends Iterable<K> {
	
	/** Enqueue a new element at the end of the queue */
	public void enqueue(K item);
	
	/** Dequeue the "first" element in the queue
	 * @return "first" element or null if it doesn't exist
	 */
	public K dequeue();
	
	/** Evaluate if the queue is empty. 
	 * @return true if the queue is empty. false in other case.
	 */
	public boolean isEmpty();
	
}
