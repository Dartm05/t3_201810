package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String taxi_id;
	private String company;
	
	public Taxi()
	{
		
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
	
		return taxi_id;
	}
	
	
	public void setTaxiId(String nuevo) {
		this.taxi_id= nuevo;

	}

	/**
	 * @return company
	 */
	public void setCompany(String nuevo) {
		this.company= nuevo;

	}
	
	
	
	public String getCompany() {
	
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
}
