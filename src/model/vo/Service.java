package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	
	
	private String tripId;
	private String taxiId;
	
	
	private int tripSeconds;
	private double tripMiles;
	private double tripTotal;
	//time
	private String startTime;
	private String endTime;
	//tract
	private String dropTract;
	private String pickTract;
	
	private double latitudeP;
	private double longP;
	
	private double latitudedrop;
	private double longdrop;
	
	private String payment;
	private String typeLocPick;
	private String typeLocDrop;
	
	private double latloc;
	private double lonloc;
	private double latlocP;
	private double lonlocP;

	//community
	private int dropcom;
	private int pickcom;
	
	//fares
	private int tolls;
	private int fare;
	private int extras;
	private int tips;
	
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		return tripId;
	}	

	public void setTripId(String id) {
		this.tripId= id;
	}	
	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		return taxiId;
	}	
	
	
	public void setTaxiId(String id) {
		this.taxiId=id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		 return tripSeconds;
	}
	
	
	public void setTripSeconds(int sec) {
		this.tripSeconds=sec;
	}
	

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		return tripMiles;
	}
	
	
	public void setTripMiles(double miles) {
		this.tripMiles= miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		return tripTotal;
	}

	
	public void setTripTotal(double tot) {
		this.tripTotal=tot;
	}
	/**
	 * @return total - Total cost of the trip
	 */
	public String getStartTime() {

		return startTime;
	}
	
	public void setStartTime(String start) {

		this.startTime=start;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String tripEnd) {
		this.endTime = tripEnd;
	}

	public String getDropTract() {
		return dropTract;
	}

	public void setDropTract(String dropTract) {
		this.dropTract = dropTract;
	}

	public String getPickTract() {
		return pickTract;
	}

	public void setPickTract(String pickTract) {
		this.pickTract = pickTract;
	}

	public double getLongP() {
		return longP;
	}

	public void setLongP(double longP) {
		this.longP = longP;
	}

	public double getLatitudedrop() {
		return latitudedrop;
	}

	public void setLatitudedrop(double latitudedrop) {
		this.latitudedrop = latitudedrop;
	}

	public int getTolls() {
		return tolls;
	}

	public void setTolls(int tolls) {
		this.tolls = tolls;
	}

	public int getFare() {
		return fare;
	}

	public void setFare(int fare) {
		this.fare = fare;
	}

	public int getPickcom() {
		return pickcom;
	}

	public void setPickcom(int pickcom) {
		this.pickcom = pickcom;
	}

	public int getDropcom() {
		return dropcom;
	}

	public void setDropcom(int dropcom) {
		this.dropcom = dropcom;
	}

	public int getExtras() {
		return extras;
	}

	public void setExtras(int extras) {
		this.extras = extras;
	}

	public double getLongdrop() {
		return longdrop;
	}

	public void setLongdrop(double longdrop) {
		this.longdrop = longdrop;
	}

	public double getLatitudeP() {
		return latitudeP;
	}

	public void setLatitudeP(double latitudeP) {
		this.latitudeP = latitudeP;
	}

	public int getTips() {
		return tips;
	}

	public void setTips(int tips) {
		this.tips = tips;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTypeLocPick() {
		return typeLocPick;
	}

	public void setTypeLocPick(String typeLocPick) {
		this.typeLocPick = typeLocPick;
	}

	public String getTypeLocDrop() {
		return typeLocDrop;
	}

	public void setTypeLocDrop(String typeLocDrop) {
		this.typeLocDrop = typeLocDrop;
	}

	public double getLatloc() {
		return latloc;
	}

	public void setLatloc(double latloc) {
		this.latloc = latloc;
	}

	public double getLonloc() {
		return lonloc;
	}

	public void setLonloc(double lonloc) {
		this.lonloc = lonloc;
	}

	public double getLatlocP() {
		return latlocP;
	}

	public void setLatlocP(double latlocP) {
		this.latlocP = latlocP;
	}

	public double getLonlocP() {
		return lonlocP;
	}

	public void setLonlocP(double lonlocP) {
		this.lonlocP = lonlocP;
	}
}
