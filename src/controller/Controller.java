package controller;

import api.ITaxiTripsManager;
import model.logic.TaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	private static ITaxiTripsManager  manager = new TaxiTripsManager();
	
	/** To load the services of the taxi with taxiId */
	public static void loadServices( String taxiId ) {
		// To define the dataset file's name 
		String serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";
		
		manager.loadServices( serviceFile, taxiId );
	}
		
	public static int [] servicesInInverseOrder(String taxId) {
		return manager.servicesInInverseOrder(taxId);
	}
	
	public static int [] servicesInOrder(String taxi) {
		return manager.servicesInOrder(taxi);
	}
}
